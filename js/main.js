(function($) {



    var form = $("#signup-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
            element.before(error);
        },
        rules: {
            email: {
                email: true
            }
        },
        onfocusout: function(element) {
            $(element).valid();
        },
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        stepsOrientation: "vertical",
        titleTemplate: '<div class="title"><span class="step-number">#index#</span><span class="step-text">#title#</span></div>',
        labels: {
            previous: 'Previous',
            next: 'Next',
            finish: 'Finish',
            current: ''
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            if (currentIndex === 0) {
                form.parent().parent().parent().append('<div class="footer footer-' + currentIndex + '"></div>');
            }
            if (currentIndex === 1) {
                form.parent().parent().parent().find('.footer').removeClass('footer-0').addClass('footer-' + currentIndex + '');
            }
            if (currentIndex === 2) {
                form.parent().parent().parent().find('.footer').removeClass('footer-1').addClass('footer-' + currentIndex + '');
            }
            if (currentIndex === 3) {
                form.parent().parent().parent().find('.footer').removeClass('footer-2').addClass('footer-' + currentIndex + '');
            }
            // if(currentIndex === 4) {
            //     form.parent().parent().parent().append('<div class="footer" style="height:752px;"></div>');
            // }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            alert('azs');
            // var nom_societe= $("#nom_societe").val();
            // var nom_compagne = $("#nom_compagne").val();
            // var select_type = $(".select-type").val();
            // var date_deb = $("#date_deb").val();
            // var date_fin = $("#date_fin").val();

            // var zone = $("#zone").val();
            // var lev = $("#lev").val();
            // var deb_diff=$("#deb_diff").val();
            // var fin_diff= $("#fin_diff").val();
            // var cible = $("#cible").val();
            // var budget_vendu= $("#budget_vendu").val();
            // var budget_reel=$("#budget_reel").val();
            // $.ajax("insert_global.php",{
            //     method: "POST",
            //     data: {
            //         nom_societe:nom_societe,
            //         nom_compagne:nom_compagne,
            //         select_type:select_type,
            //         date_deb:date_deb,
            //         date_fin:date_fin,
            //         zone:zone,
            //         lev:lev,
            //         deb_diff:deb_diff,
            //         fin_diff:fin_diff,
            //         cible:cible,
            //         budget_vendu:budget_vendu,
            //         budget_reel:budget_reel
            //     },
            //     success: function(data){
            //         console.log(data);
            //     },
            //     error: function(){
            //         console.log("err");
            //     }

            // });
            
        },
        onStepChanged: function(event, currentIndex, priorIndex) {

            return true;
        }
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "",
        remote: "",
        email: "",
        url: "",
        date: "",
        dateISO: "",
        number: "",
        digits: "",
        creditcard: "",
        equalTo: ""
    });

    $.dobPicker({
        daySelector: '#birth_date',
        monthSelector: '#birth_month',
        yearSelector: '#birth_year',
        dayDefault: '',
        monthDefault: '',
        yearDefault: '',
        minimumAge: 0,
        maximumAge: 120
    });
    var marginSlider = document.getElementById('slider-margin');
    if (marginSlider != undefined) {
        noUiSlider.create(marginSlider, {
              start: [1100],
              step: 100,
              connect: [true, false],
              tooltips: [true],
              range: {
                  'min': 100,
                  'max': 2000
              },
              pips: {
                    mode: 'values',
                    values: [100, 2000],
                    density: 4
                    },
                format: wNumb({
                    decimals: 0,
                    thousand: '',
                    prefix: '$ ',
                })
        });
        var marginMin = document.getElementById('value-lower'),
	    marginMax = document.getElementById('value-upper');

        marginSlider.noUiSlider.on('update', function ( values, handle ) {
            if ( handle ) {
                marginMax.innerHTML = values[handle];
            } else {
                marginMin.innerHTML = values[handle];
            }
        });
    }


    $(".etap").on("change",function(){
        if($(this).val()==="1"){
            $('#proj').fadeIn(100);
  $("body").css("overflow", "hidden");
  return false;
        }else if($(this).val()==="2"){
         $("#modfi_visu").show();
        }else if($(this).val()==="3"){
            $("#prop_setup").show();
        }else if($(this).val()==="4"){
            $("#modif_setup").show();
        }else if($(this).val()==="5"){
            $("#mise_sit").show();
        }else if($(this).val()==="6"){
            $("#go_client").show();
        }else if($(this).val()==="7"){
            $("#parametrage").show();
        }else if($(this).val()==="8"){
            $("#deamnde_lancement").show();
        }else if($(this).val()==="9"){
            $("#lancement").show();
        }else if($(this).val()==="10"){
            $("#optimisation").show();
        }else if($(this).val()==="11"){
            $("#envoie_point").show();
        }else if($(this).val()==="12"){
            $("#bilan_interm").show();
        }else if($(this).val()==="13"){
            $("#bilan_final").show();
        }
    })




    $('.fox-close-btn').on('click', function(){
        $('#proj').hide();
        $("#modfi_visu").hide();
        $("#prop_setup").hide();
        $("#modif_setup").hide();
        $("#mise_sit").hide();
        $("#go_client").hide();
        $("#parametrage").hide();
        $("#optimisation").hide();
        $("#envoie_point").hide();
        $("#bilan_interm").hide();
        $("#bilan_final").hide();
        $("body").css("overflow", "auto");
        return false;
      })

      $("#close_dif").on("click",function(){
        $('#difus').hide();
      })

    $(".tst").on("click",function(){
        $(this).removeClass("tst");
       $(this).addClass("next2");
    });

   
$(".btn_etap_2").on("click", function(){
    alert($("#lev").val());
});
    
$(".plus").on("click", function(){
    $('#difus').show();
})



//FICHE AJAX

$(document).on("change", "#pet-select" ,function(){
    if($(this).val()==="autre"){
        $(this).replaceWith("<div class='rem'><div class='form-flex'><div class='form-group form-flex'><input type='text' name='type'  class='select-type'><span class='x'>X</span></div></div></div>");
    }
})

$(document).on("click" ,"span.x", function(){

    $(".rem").replaceWith("<div class='form-flex'><div class='form-group form-flex'><select name='type' id='pet-select' class='select-type'><option value='Market'>Market</option><option value='Recrutement'>Recrutement</option><option value='autre'>Autre</option></select></div></div>")
});

$(".btn_save_fiche").on("click", function(){
    var nom_societe= $("#nom_societe").val();
    var nom_compagne = $("#nom_compagne").val();
    var select_type = $(".select-type").val();
    var date_deb = $("#date_deb").val();
    var date_fin = $("#date_fin").val();
    
    if(nom_societe==="" || nom_compagne ==="" || select_type==="" || date_deb==="" || date_fin===""){
        $(".err_fiche").show();
    }else{
        $(".err_fiche").hide();
        $.ajax("insert_fihce.php",{
            method: "POST",
            data: {
                nom_societe:nom_societe,
                nom_compagne:nom_compagne,
                select_type:select_type,
                date_deb:date_deb,
                date_fin:date_fin,
                
            },
    
            success: function(data){
                // var duree = $("#duree").val("");
                // var duree = $("#duree").val(data);
    
            var  id = $(".id_cam").attr('data-id' , data.replace(/\s/g, ''));
                modfiche();
                
                // $(".alert-success").html("Le visuel a ete bien enregistrer").show()
                // setTimeout(function(){   $(".alert-success").html("").hide(); }, 2000);
              
            },
            error:function(){
                console.log("error");
            }
        })
    }

});


//MODIF FUCHELISTE
function modfiche(){
    var id_com = $(".id_cam").attr("data-id");
    $.ajax("testmodif.php",{
        method: "post",
        data: {
            id_com:id_com,
        },
        success:function(data){
            $(".rempalce").html("");
            $(".rempalce").replaceWith(data);
            $("#mod").show();
        },
        error:function(){
            console.log("err");
        }
    });
}



//MODIF
$(".mod_fiche").on("click", function(){
 ;
    var nom_societe= $("#nom_societe").val();
    var nom_compagne = $("#nom_compagne").val();
    var select_type = $(".select-type").val();
    var date_deb = $("#date_deb").val();
    var date_fin = $("#date_fin").val();
    var id_com = $(".id_cam").attr("data-id");
    $.ajax("controller_modif_fiche.php",{
        method: "POST",
        data: {
            nom_societe:nom_societe,
            nom_compagne:nom_compagne,
            select_type:select_type,
            date_deb:date_deb,
            date_fin:date_fin,
            id_com:id_com
        },

        success: function(){
            alert("suc")
        },
        error:function(){
            alert("err");
        }




    });
  

  
});



//RECA
$(".save_diff_btn").on("click", function(){
    var id_com = $(".id_cam").attr("data-id");
    $.ajax("recap_list.php",{
        
        method: "POST",
        data: {
            id_com:id_com
    
        },

        success: function(data){
           console.log(data);
           $(".list_rcap").html("");
            $(".list_rcap").append(data);
        }

});
});



//DIFF
$(".btn_save_diff").on("click", function(){
    var zone = $("#zone").val();
    var lev = $("#lev").val();
    var deb_diff=$("#deb_diff").val();
    var fin_diff= $("#fin_diff").val();
    var cible = $("#cible").val();
    var budget_vendu= $("#budget_vendu").val();
    var budget_reel=$("#budget_reel").val();
    var id_com= $(".id_cam").attr("data-id");
    if(zone ===""){
        alert("vide");
    }else{
        $.ajax("test2.php",{
            method: "POST",
            data: {
                zone:zone,
                lev:lev,
                deb_diff:deb_diff,
                fin_diff:fin_diff,
                cible:cible,
                budget_vendu:budget_vendu,
                budget_reel:budget_reel,
                id_com:id_com
    
            },
    
            success: function(){
           
                var zone = $("#zone").val("");
                var lev = $("#lev").val("");
                var deb_diff=$("#deb_diff").val("");
                var fin_diff= $("#fin_diff").val("");
                var cible = $("#cible").val("");
                var budget_vendu= $("#budget_vendu").val("");
                var budget_reel=$("#budget_reel").val("");
              
              
            },
            error:function(){
                console.log("error");
            }
        })
    
    }
});

$("input[name=zone]").on("keydown", function(event){
   var val = $(this).val();
  

  var arr = new Array();


 if(val !=="" && event.which === 13){
    var div = "<div class='jopa'>";
    
    $("input[name=zone]").each(function(i){
        arr.push($(this).val());
       
        $(".la").append("<div class='alert buls' role='alert'><b>" + arr[i]  + "</b></div>");
       
        
    })
   
}  
   });

  

    // if(val !=="" && event.which === 13){
    //     Object.keys(val).forEach(function(jB){
    //         var arr = val[jB];
    //         $(".la").append("<b>" + val + "<b><br>");
    //       });
    //   $(".la").append($(this).val());
    //   $(this).val("");
    // }


$(".save_diff_btn").on("click", function(){
    var zone = $("input[name=zone]").val();
    var lev = $(".lev").val();
    var deb_diff=$("input[name=debut_diff]").val();
    var fin_diff=$("input[name=fin_diff]").val();
    var cible = $("input[name=cible]").val();
    var budget_vendu=  $("input[name=budget_vendu]").val();
    var budget_reel= $("input[name=budget_reel]").val();
    var id_com = $(".id_cam").attr("data-id");
    if(zone ===""){
        alert("vide");
     
    }else{
        $.ajax("test2.php",{
            method: "POST",
            data: {
                zone:zone,
                lev:lev,
                deb_diff:deb_diff,
                fin_diff:fin_diff,
                cible:cible,
                budget_vendu:budget_vendu,
                budget_reel:budget_reel,
                id_com:id_com
    
            },
    
            success: function(){
                var zone = $("input[name=zone]").val("");
                var lev = $("input[name=levier]").val("");
                var deb_diff=$("input[name=debut_diff]").val("");
                var fin_diff=$("input[name=fin_diff]").val("");
                var cible = $("input[name=cible]").val("");
                var budget_vendu=  $("input[name=budget_vendu]").val("");
                var budget_reel= $("input[name=budget_reel]").val("");
              
            },
            error:function(){
                console.log("error");
            }
        })
    }

});



//PRJERT AJAX
$(".btn_save").on("click", function(){
    var date = $("#date_env").val();
    var nb_vis = $("#nb_vis").val();
    var ann = $("#ann_projet").val();
    var id_com = $(".id_proj").attr("data-id");
    if(date ==="" || nb_vis ==="" || ann===""){
        alert("vide");
    }else{
        $.ajax("test.php",{
            method: "POST",
            data: {
                date:date,
                nb_vis:nb_vis,
                ann:ann,
                id_com:id_com
            },
    
            success: function(){
                var date = $("#date_env").val("");
                var nb_vis = $("#nb_vis").val("");
                var ann = $("#ann_projet").val("");
                
                $(".alert-success").html("Le visuel a ete bien enregistrer").show()
                setTimeout(function(){   $(".alert-success").html("").hide(); }, 2000);
              
            },
            error:function(){
                console.log("error");
            }
        })
    }



 



});


    //MODIF VISUELE

    $(".btn_mdoif_vis").on("click", function(){
    var date = $("#date_modif").val();
    var nb_vis = $("#nb_vis_modif").val();
    var ann = $("#ann_viusem_modif").val();
    var id_com = $(".id_proj").attr("data-id");
    if(date ==="" || nb_vis ==="" || ann===""){
        alert("vide");
    }else{
        $.ajax("controller_modif_visu.php",{
            method: "POST",
            data: {
                date:date,
                nb_vis:nb_vis,
                ann:ann,
                id_com:id_com
            },
    
            success: function(){
                var date = $("#date_env").val("");
                var nb_vis = $("#nb_vis").val("");
                var ann = $("#ann_projet").val("");
                
                $(".alert-success").html("Le visuel a ete bien enregistrer").show()
                setTimeout(function(){   $(".alert-success").html("").hide(); }, 2000);
              
            },
            error:function(){
                console.log("error");
            }
        })
    }


    })
  //MODIF VISUELE




$(".btn_save_proj").on("click", function(){
    var nom_contact = $("input[name=nom_contact]").val();
    var prise_contact = $("input[name=prise_contact]").val();
    var comment = $(".coment").val();
    var id_com = $(".id_cam").attr("data-id");
    $.ajax("insert_projet_ajax.php",{
        method: "POST",
        data: {
            nom_contact:nom_contact,
            prise_contact:prise_contact,
            comment:comment,
            id_com:id_com
        },

        success: function(data){
            var nom_contact = $("input[name=nom_contact]").val("");
            var prise_contact = $("input[name=prise_contact]").val("");
            var comment = $(".coment").val("");
            var  id = $(".id_proj").attr('data-id' , data.replace(/\s/g, ''));
        },
        error:function(){
            console.log("error");
        }
    });

})




//Prop setup
$(".btn_save_porop_setup").on("click", function(){
 
    var date = $("#date_prop").val();
    var comment = $("#ann_setup").val();
    var id_com = $(".id_proj").attr("data-id");
    $.ajax("controller_insert_setup.php",{
        method: "POST",
        data: {
          date:date,
          comment:comment,
          id_com:id_com
        },

        success: function(data){
            var nom_contact = $("input[name=nom_contact]").val("");
            var prise_contact = $("input[name=prise_contact]").val("");
            var comment = $(".coment").val("");
            var  id = $(".id_proj").attr('data-id' , data.replace(/\s/g, ''));
        },
        error:function(){
            console.log("error");
        }
    });

})


//MISE SIT  


$(".btn_save_mise_sit").on("click", function(){
    var date = $("#date_mise_sit").val();
    var comment = $("#ann_mise_sit").val();
    var id_com = $(".id_proj").attr("data-id");
    $.ajax("contreoller_insert_mise_sit.php",{
        method: "POST",
        data: {
          date:date,
          comment:comment,
          id_com:id_com
        },

        success: function(data){
            var nom_contact = $("input[name=nom_contact]").val("");
            var prise_contact = $("input[name=prise_contact]").val("");
            var comment = $(".coment").val("");
            var  id = $(".id_proj").attr('data-id' , data.replace(/\s/g, ''));
        },
        error:function(){
            console.log("error");
        }
    });
})


//GO CLIENT


})(jQuery);

// $(document).ready(function(){
//     $("input[name=zone]").on("change", function(){
//         var zones =  $(this).val();
//        zones.forEach(function(item){
//           console.log(item);
       
//         });
          
//       })
// })