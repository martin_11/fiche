<?php


function countDaysMonth($datestart, $datefin){
    $date1 = strtotime($datestart);  
    $date2 = strtotime($datefin);
  
// Formulate the Difference between two dates 
$diff = abs($date2 - $date1);  
  
  
// To get the year divide the resultant date into 
// total seconds in a year (365*60*60*24) 
$years = floor($diff / (365*60*60*24));  
  
  
// To get the month, subtract it with years and 
// divide the resultant date into 
// total seconds in a month (30*60*60*24) 
$months = floor(($diff - $years * 365*60*60*24) 
                               / (30*60*60*24));
 $days = floor(($diff - $years * 365*60*60*24 -  
                               $months*30*60*60*24)/ (60*60*24)); 

return  $months . " mois et " . $days . " jours";
}