-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 23 juil. 2020 à 15:42
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `fiche`
--

-- --------------------------------------------------------

--
-- Structure de la table `diffusion`
--

DROP TABLE IF EXISTS `diffusion`;
CREATE TABLE IF NOT EXISTS `diffusion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fiche` int(11) NOT NULL,
  `zone` varchar(70) NOT NULL,
  `debut` date NOT NULL,
  `fin` date NOT NULL,
  `levier` varchar(70) NOT NULL,
  `cibles` varchar(50) NOT NULL,
  `budget_vendu` decimal(10,2) NOT NULL,
  `budget_reel` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fiche` (`fiche`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `diffusion`
--

INSERT INTO `diffusion` (`id`, `fiche`, `zone`, `debut`, `fin`, `levier`, `cibles`, `budget_vendu`, `budget_reel`) VALUES
(1, 1, '1', '2020-07-22', '2020-07-22', 'fb', 'JN', '250.00', '250.00'),
(2, 1, '2', '2020-07-22', '2020-07-22', 'fb', 'JN', '250.00', '250.00'),
(3, 1, '12', '2020-07-22', '2020-07-22', 'fb', 'JN', '250.00', '250.00'),
(4, 1, '', '2020-07-22', '2020-07-22', 'fb', 'JN', '250.00', '250.00'),
(5, 1, '50', '2020-07-22', '2020-07-22', 'fb', 'JN', '250.00', '250.00'),
(6, 13, '20', '2020-07-22', '2020-07-22', 'fb', 'JN', '250.00', '250.00'),
(7, 13, '80', '2020-07-22', '2020-07-22', 'fb', 'JN', '250.00', '250.00'),
(8, 1, '8056456', '2020-07-22', '2020-07-22', 'fb', 'JN', '250.00', '250.00');

-- --------------------------------------------------------

--
-- Structure de la table `fiche_campagne`
--

DROP TABLE IF EXISTS `fiche_campagne`;
CREATE TABLE IF NOT EXISTS `fiche_campagne` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_societe` varchar(70) NOT NULL,
  `nom_compagne` varchar(70) NOT NULL,
  `type` varchar(20) NOT NULL,
  `debut` date NOT NULL,
  `fin` date NOT NULL,
  `duree` varchar(20) NOT NULL,
  `cout_final` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fiche_campagne`
--

INSERT INTO `fiche_campagne` (`id`, `nom_societe`, `nom_compagne`, `type`, `debut`, `fin`, `duree`, `cout_final`) VALUES
(1, 'test-ss', 'testcomp', 'testtype', '2019-10-28', '2020-10-28', '1 mois', '250.00'),
(2, 'test', 'testcomp', 'testtype', '2019-10-28', '2020-10-28', '1 mois', '250.00'),
(3, 'test', 'testcomp', 'testtype', '2019-10-28', '2020-10-28', '1 mois', '250.00'),
(5, 'Palaco', 'Palacoss', 'Market', '2020-07-23', '2020-08-24', '1mois et 2jours', '0.00'),
(6, 'LOREM', 'OPSUM', 'Recrutement', '2020-07-23', '2020-07-30', '0 mois et 7 jours', '0.00'),
(7, 'LOREM', 'OPSUM', 'Recrutement', '2020-07-23', '2020-07-30', '0 mois et 7 jours', '0.00'),
(8, 'AHAHAHA', 'AJAJAJA', 'Market', '2020-07-23', '2020-07-30', '0 mois et 7 jours', '0.00'),
(9, 'qdqsd', 'qsdqsd', 'Market', '2020-07-24', '2020-07-24', '0 mois et 0 jours', '0.00'),
(10, 'test', 'test', 'Market', '2020-07-23', '2020-07-30', '0 mois et 7 jours', '0.00'),
(11, 'Compa', 'qsdqs', 'Market', '2020-07-24', '2020-07-30', '0 mois et 6 jours', '0.00'),
(12, 'tyest', 'tse', 'Market', '2020-07-23', '2020-08-23', '1 mois et 1 jours', '0.00'),
(13, 'PALACO', 'CHAZOT', 'Market', '2020-07-22', '2020-07-26', '0 mois et 4 jours', '0.00');

-- --------------------------------------------------------

--
-- Structure de la table `point`
--

DROP TABLE IF EXISTS `point`;
CREATE TABLE IF NOT EXISTS `point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projet` int(11) NOT NULL,
  `date` date NOT NULL,
  `comment` varchar(100) NOT NULL,
  `done` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projet` (`projet`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fiche` int(11) NOT NULL,
  `prise_contact` date NOT NULL,
  `comment` text NOT NULL,
  `done` tinyint(1) NOT NULL,
  `mise_situation` date NOT NULL,
  `parametrage` date NOT NULL,
  `demande_lancement` date NOT NULL,
  `lancement` date NOT NULL,
  `bilan_interm` varchar(100) NOT NULL,
  `bilan_final` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fiche` (`fiche`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `set_up`
--

DROP TABLE IF EXISTS `set_up`;
CREATE TABLE IF NOT EXISTS `set_up` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projet` int(11) NOT NULL,
  `date` date NOT NULL,
  `comment` varchar(100) NOT NULL,
  `done` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projet` (`projet`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `visuel`
--

DROP TABLE IF EXISTS `visuel`;
CREATE TABLE IF NOT EXISTS `visuel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projet` int(11) NOT NULL,
  `proposition_visuel` date NOT NULL,
  `comment` varchar(100) NOT NULL,
  `nb` varchar(50) NOT NULL,
  `done` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projet` (`projet`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `visuel`
--

INSERT INTO `visuel` (`id`, `projet`, `proposition_visuel`, `comment`, `nb`, `done`) VALUES
(1, 1, '2020-07-24', 'sss', '4', 1),
(2, 1, '2020-07-24', 'LOREM ipserty', '5', 1),
(3, 1, '2020-07-15', 'sss', '10', 1),
(4, 1, '2020-07-24', 'ssqqsdqsd', '15', 1),
(5, 1, '2020-07-10', 'sdsd', '41', 1),
(6, 1, '2020-07-02', 'qsdqsd', '4', 1),
(7, 1, '2020-07-24', 'sdfsdfsfsdf', '10', 1),
(8, 1, '2020-07-24', 'qsdqsd', '45', 1),
(9, 1, '2020-07-26', 'sdsd', '502', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
