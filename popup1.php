
<div class="btn f-right" id="fox-popup-triger">
  <p>Click Me</p>
</div>
<div class="fox-popup-wrap">
  <div class="fox-close-btn">
    <div class="bar"></div>
    <div class="bar"></div>
  </div>
  <div class="fox-form-wrap">
    <form>
      <div class="col col-md-6">
        <div class="form-group">
          <label for="email">* Email:</label>
          <input class="form-control" id="email" type="email"/>
        </div>
      </div>
      <div class="col col-md-6">
        <div class="form-group">
          <label for="tel">Tel:</label>
          <input class="form-control" id="email" type="tel"/>
        </div>
      </div>
      <div class="col col-md-6"></div>
      <div class="col col-md-6">
        <div class="form-group">
          <label for="tel">* Realizacja do dnia:</label>
          <input class="form-control" id="email" type="date"/>
        </div>
      </div>
      <div class="col col-md-12">
        <div class="form-group">
          <label for="wycena">* Przedmiot wyceny:</label>
          <input class="form-control" id="email" type="text" placeholder="Wpisz co chcesz wycenić"/>
        </div>
      </div>
      <div class="col col-md-12">
        <div class="form-group">
          <label for="wycena">* Wymagania do zamówienia</label>
          <textarea class="form-control" id="email" rows="10" placeholder="Informacje takie: ilość, materiały, wymiary, jakość, inne wymagania ..."></textarea>
        </div>
      </div>
      <div class="form-group">
        <div class="col col-md-4"> 
          <div class="checkbox">
            <label>
              <input type="checkbox" checked="checked" disabled="disabled"/>Wyrażam zgodę na kontakt w sprawie niniejszej wyceny
            </label>
          </div>
        </div>
        <div class="col col-md-4"> 
          <div class="checkbox">
            <label>
              <input type="checkbox" checked="checked"/>Wyrażam zgodę na otrzymywanie informacji promocyjnych
            </label>
          </div>
        </div>
        <div class="col col-md-4"> 
          <div class="checkbox">
            <label>
              <input type="checkbox"/>Nie jestem do końca pewien czego szukam. Proszę o kontakt i doradztwo.
            </label>
          </div>
        </div>
      </div>
      <div class="col col-md-12"> 
        <button class="btn btn-default f-right" type="submit">Wyceń zamówienie</button>
      </div>
    </form>
    <p class="info">* informacje które otrzymujemy nie są sprzedawane innym podmiotom ani też wykorzystywane przez osoby trzecie w celu innym niż opisano w regulaminie. Wysyłając zapytanie jednoczesnie deklarujesz akceptację regulaminu który znajdziesz na stronie <a href="">regulamin</a>.</p>
  </div>
</div>


<style>
    * {
  -webkit-transition: 1s;
  transition: 1s;
  padding: 0;
  margin: 0;
}

.f-right {
  float: right;
}

.f-left {
  float: left;
}

body {
  height: 2000px;
}

.btn#fox-popup-triger {
  padding: 20px;
  margin: 10px;
  border: 1px solid #333;
  width: 200px;
  text-align: center;
  background-color: #053;
  color: #fff;
}
.btn#fox-popup-triger p {
  margin: 0;
  padding: 0;
}

.fox-popup-wrap {
  display: none;
  background-color: #006464;
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  overflow: auto;
}

.fox-close-btn {
  width: 40px;
  height: 40px;
  background-color: #c80000;
  position: fixed;
  top: 10px;
  right: 30px;
  border-radius: 50%;
  cursor: pointer;
}
.fox-close-btn:hover {
  background-color: #555;
}
.fox-close-btn .bar {
  border-radius: 50%;
  position: absolute;
  top: 45%;
  left: 10%;
  width: 80%;
  height: 4px;
  background: #fff;
}
.fox-close-btn .bar:nth-child(1) {
  -webkit-transform: rotate(45deg);
          transform: rotate(45deg);
}
.fox-close-btn .bar:nth-child(2) {
  -webkit-transform: rotate(-45deg);
          transform: rotate(-45deg);
}

.fox-form-wrap {
  position: absolute;
  top: 10%;
  right: 20%;
  left: 20%;
}
.fox-form-wrap form {
  max-width: 900px;
  margin: auto;
}
.fox-form-wrap form p {
  color: white;
}
.fox-form-wrap form label {
  color: white;
}
.fox-form-wrap form .btn {
  padding: 10px;
}
.fox-form-wrap form .btn:hover {
  -webkit-transform: scale(1.1);
          transform: scale(1.1);
}
.fox-form-wrap p.info {
  font-size: 1.1rem;
  position: absolute;
  bottom: -110px;
  left: 0px;
  color: #fff;
}

</style>