<?php

//CLASS principale 

class principal {
    
    protected $pk; //Cle primaire
    protected $table; //table
    protected $champs=[];//les champs du table
    protected $vlues=[]; //las valeurs 
    protected $liens = []; //les liens ex = ["user"=> "user"]
    protected $liensValues =[]; //les valeurs du liens 



   public function __construct($pk= null){
       //role charger l'objet par id apres avoire declarer
       //parametres $pk -> cles primaires
       //retour l'objet charger
       if(!is_null($pk)){
           $this->loadByPk($pk);
       }
   
   }
   
   
   public function get($champ){
    //role : 
    //parametres $champ nom du champ
    //retour
    if(!in_array($champ, $this->champs)){
        echo "Erreur champ $champ a ete pas trouver";
        return "";
    }
    if(isset($this->liens[$champ])){
        if(!isset($this->liensValues[$champ])){
            $class = $this->liens[$champ];
            $this->liensValues[$champ] = new $class($this->values[$champ]);
        }
        return $this->liensValues[$champ];
    }
    if(isset($this->values[$champ])){
        return $this->values[$champ];
    }else{
        return "";
    }
   }



   public function set($champ, $val){
       //role init et modifier lobjet
       //paramtres $champ nom du champ, $val valuer
       //retour ture ou false
       if(!in_array($champ, $this->champs)){
           echo "Champ $champ a ete pas trouver";
           return false;
       }
       $this->values[$champ]= $val;
       return true;
   }


   public function loadByPk($pk){
       //role charger lobjet par id
       //paramtres pk cle primar
       //retour true ou false
       $sql = "SELECT * FROM `{$this->table}` WHERE `{$this->pk}` =:pk";
       $param = [":pk"=>$pk];
       $req = BDDselect($sql, $param);
       $ligne = $req->fetch(PDO::FETCH_ASSOC);
       if(!empty($ligne)){
           $this->setFromTab($ligne);
           return true;
       }else{
           $this->values[$this->pk] = 0;
           return false;
       }
   }


   public function setFromTab($tab){
       //role : charger l'objet avec plusirs attributes
       //$tab = tableu
       //retour true ou false
       foreach($this->champs as $champ){
           if(isset($tab["$champ"])){
               $this->set($champ, $tab["$champ"]);
           }
       }
       return true;
   }



   public function insert(){
       //role inseret dans la base de donnes
       //paramert non
       //retour true ou false
       $sql = "INSERT INTO `{$this->table}` SET ";
       $set = [];
       $param = [];
       foreach($this->champs as $champ){
           if($champ !=="id"){
            $set[] = "`$champ`=:$champ";
            $param[":$champ"] = $this->values[$champ];
           }
           
       }

       $sql .=  implode(",", $set);
       if(BDDquery($sql, $param)===1){
           $this->values[$this->pk] = BDDlastId();
           return true;
       }else{
           echo "Erreur d'insertion " . get_class($this);
           return false;
       }

   }

 
   public function update(){
       //Role : mettre a jour 
       //paramtre non
       //retour trou ou false
       $sql = "UPDATE `{$this->table}` SET ";
       $set= [];
       $param = [];
       foreach($this->champs as $champ){
           $set[] = "`$champ`=:$champ";
           $param[":$champ"] = $this->values[$champ];
       }
       $sql .= implode(",", $set);
       $sql .=" WHERE `{$this->pk}`=:pk";
       $param[":pk"]= $this->values[$this->pk];
       if(BDDselect($sql, $param)!==-1){
           return true;
       }else{
           echo "Erreur de modification " . get_class($this);
           return false;
       }
   }



   public function delete(){
       //role suprimer 
       //paramtres non
       //retour true ou false
       $sql = "DELETE FROM `{$this->table}` WHERE `{$this->pk}`=:pk";
       $param =[":pk"=>$this->values[$this->pk]];
       if(BDDquery($sql, $param)!==-1){
           return true;
       }else{
           echo "Erreur de delete " . get_class($this);
           return false;
       }

   }



   public function liste(){
       //Role affiche liste d'objet
       //paramtres non
       //retour tableu
       $class  = get_class($this);
       $sql ="SELECT * FROM `{$this->table}`";
       $param = [""];
       $req = BDDselect($sql, $param);
       $result = [];
       while($ligne = $req->fetch(PDO::FETCH_ASSOC)){
           $class = new $class();
           $class->setFromTab($ligne);
           $result[$ligne["id"]] = $class;
       }

       return $result;
   }



}


?>

