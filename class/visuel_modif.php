<?php



class visuel_modif extends principal{
    protected $pk = "id";
    protected $table = "visuel_modif";
    protected $champs = ["id", "projet","date_modif" ,"nb_visuel" , "annotation"];
    protected $liens = ["projet"=>"projet"];
}