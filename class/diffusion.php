<?php


class diffusion extends principal {
    protected $pk = "id";
    protected $table = "diffusion";
    protected $champs = ["id", "fiche","zone" ,"debut" , "fin", "levier", "cibles","budget_vendu", "budget_reel"];
    protected $liens = ["fiche"=>"fiche"];



    public function getDiff($fiche){
        $sql = "SELECT * FROM `{$this->table}` WHERE `fiche`=:fiche";
        $param = [":fiche"=>$fiche];
        $req = BDDselect($sql, $param);
       $result = [];
       while($ligne = $req->fetch(PDO::FETCH_ASSOC)){
           $dif = new diffusion();
           $dif->setFromTab($ligne);
           $result[$ligne["id"]] = $dif;
       }

       return $result;

    }

    
}