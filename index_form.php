<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="colorlib.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="vendor/nouislider/nouislider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Main css -->
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&display=swap" rel="stylesheet">
    
</head>

<body>

    <div class="main">

        <div class="container">
            <form method="POST" id="signup-form" class="signup-form" action="#">
                <div >
                    <h3>FICHE CAMPAGNE</h3>
                    <fieldset>
                    <div class="rempalce">
                        <h2>FICHE CAMPAGNE</h2>
                        <p class="desc">Info campagne</p>
                        <div class="fieldset-content">
                      

                            <div class="form-row">
                            
                                <label class="form-label"></label>
                                <div class="form-flex">
                                    <div class="form-group form-flex">
                                        
                                        <p class="text-input">NOM SOCIÉTÉ</p>
                                        <input type="text" name="nom_societe" id="nom_societe" />
                                    </div>
                                    <div class="form-group form-flex">
                                        <p class="text-input">NOM CAMPAGNE</p>
                                        <input type="text" name="nom_compagne" id="nom_compagne" />
                                        
                                    </div>
                                </div>
                            </div>

                            
                                <div class="form-flex">
                                    <div class="form-group form-flex">
                                        
                                        <p class="text-input" style="width: 18% !important;">TYPE</p>
                                        <select name="type" id="pet-select" class="select-type">
                                           
                                            <option value="Market">Market</option>
                                            <option value="Recrutement">Recrutement</option>
                                            <option value="autre">Autre</option>
                                        
                                        </select>
                                    </div>
                                 
                                </div>



                                
                           
        



                                <div class="form-flex">
                                    <div class="form-group form-flex">
                                        
                                        <p class="text-input" style="width: 18% !important;">DATE DE VALIDATION DEVIS</p>
                                        <input type="date" name="debut" id="date_deb">
                                    </div>
                                 
                                </div>


                                <div class="form-flex">
                                    <div class="form-group form-flex">
                                        
                                        <p class="text-input" style="width: 18% !important;">DATE DE LANCEMENT PREVISIONNELE</p>
                                        <input type="date" name="fin" id="date_fin">
                                    </div>
                                 
                                </div>

                            

                               

                            <!-- <div class="form-group">
                                
                                <input type="text" name="cout_final" id="cout" />
                                <span class="text-input">Coût final</span>
                            </div> -->
                            <!-- <div class="form-date">
                                <label for="birth_date" class="form-label">Birth Date</label>
                                <div class="form-date-group">
                                    <div class="form-date-item">
                                        <select id="birth_month" name="birth_month"></select>
                                        <span class="text-input">MM</span>
                                    </div>
                                    <div class="form-date-item">
                                        <select id="birth_date" name="birth_date"></select>
                                        <span class="text-input">DD</span>
                                    </div>
                                    <div class="form-date-item">
                                        <select id="birth_year" name="birth_year"></select>
                                        <span class="text-input">YYYY</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ssn" class="form-label">SSN</label>
                                <input type="text" name="ssn" id="ssn" />
                            </div> -->
                        </div>
                     

                        <div class="cont">
                          <div class="btn_save_fiche" style="float: right;">
                        <p >Enregistrer</p>
                      </div>
                      <div class="err err_fiche" role="alert">
                        <p>CHAMP(S) VIDE(S)</p>
                    </div>
                      </div>
                      </div>
                      <div class="cont" id="mod" style="display: none;">
                          <div class="mod_fiche" id="md" style="float: right;">
                        <p>Modidife</p>
                      </div>
                    </fieldset>

                    <h3>DIFFUSION</h3>
                    <fieldset>
                        <h2>DIFFUSION</h2>
                        <p class="desc">Info campagne</p>
                        <p class="la"></p>
                        <div class="fieldset-content">
                       
                                <label class="form-label"></label>
                                <div class="form-flex">
                                    <div class="form-group form-flex" style="width: 100%;">
                                        <p class="text-input" style="width: 6.5% !important; text-align: center;">ZONE</p>
                                        <input type="text" name="zone" />
                                        
                                    </div>
                         

                                    
                                </div>
                            
                         
                            <div class="form-group form-flex">
                              <p class="text-input"  style="width: 10.5% !important; text-align: center;">LEVIER</p>
                              <select name="levier" class="lev"  multiple>
                                
                                  <option value="FB">Facebook</option>
                                  <option value="insta">Instagram</option>
                                  <option value="snap">Snapchat</option>
                                  <option value="disp">Display</option>
                                  <option value="adw">Adwords</option>
                              </select>
                              
                          </div>
                            
                            <div  class="form-group form-flex">
                                <p class="text-input" style="width: 10.5% !important; text-align: center;">DÉBUT</p>
                               <input type="date" name="debut_diff" class="deb_diff">
                               
                            </div>
                            
                            <div  class="form-group form-flex">
                                <p class="text-input" style="width: 10.5% !important; text-align: center;">FIN</p>
                                <input type="date" name="fin_diff" class="fin_diff">
                               
                             </div>

                             <div class="form-group form-flex">
                                <p class="text-input" style="width: 10.5% !important; text-align: center;">CIBLES</p>
                                <input type="text" name="cible" class="cible" />
                                
                            </div>
                            <input type="hidden" name="id_cam" data-id="" class="id_cam"  >
                            <input type="hidden" name="id_cam" data-id="" class="id_proj"  >
                            <div class="form-row">
                                <label class="form-label"></label>
                                <div class="form-flex">
                                    <div class="form-group form-flex">
                                        <p class="text-input" style="width: 18% !important;">BUDGET VENDU</p>
                                        <input type="text" name="budget_vendu"  />
                                       
                                    </div>
                                    <div class="form-group form-flex">
                                        <p class="text-input" style="width: 18% !important;">BUDGET RÉEL</p>
                                        <input type="text" name="budget_reel" />
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="ici">


                            </div>

                            <div class="save_diff_btn" style="float: right;">
                              <p >Enregistrer</p>
                            </div>
                              
                             
                        </div>
                        <div class="plus">
                          <i class="fa fa-plus" aria-hidden="true"></i>
                          </div>
                    </fieldset>

                    <h3>Recap</h3>
                    <fieldset>
                        <h2>Set Financial Goals</h2>
                        <p class="desc">Set up your money limit to reach the future plan</p>
                        <div class="fieldset-content" style="width: 100% !important;">
                            <?php include "recap.php" ?>
                        </div>
                    </fieldset>


                    <h3>Projet</h3>
                    <fieldset>
                        <h2>Projet</h2>
                        <p class="desc">Set up your money limit to reach the future plan</p>
                        <div class="fieldset-content">

                            <div  class="form-group form-flex">
                                <p class="text-input" style="width: 18% !important;">NOM DU CONTACT</p>
                                <input type="text" name="nom_contact">
                               
                             </div>

                            <div  class="form-group form-flex">
                                <p class="text-input" style="width: 18% !important;">PRISE DE CONTACT</p>
                                <input type="date" name="prise_contact" p>
                              
                             </div>

                             <div  class="form-group form-flex">
                               
                                <p class="text-input" style="width: 16.5% !important;">Annotation</p>
                                <textarea name="comment" id="" cols="130" rows="10" class="coment" placeholder="Annotation"></textarea>
                              </div>
                              <div class="cont_proj">
                          <div class="btn_save_proj" style="float: right;">
                        <p >Enregistrer</p>
                      </div> 
                      <br>
                      <br>
                              <div  class="form-group form-flex" style="width: 100%; padding-top:50px">
                                <p class="text-input" style="width: 18% !important;">Étape</p>
                                <select name="etap" class="etap" id="">
                                  <option value="" disabled selected> -- ETAPE -- </option>
                                    <option value="1">Proposition de visuels</option>
                                    <option value="2">Modification de visuels</option>
                                    <option value="3">Proposition de setup</option>
                                    <option value="4">Modification de setup</option>
                                    <option value="5">Mise en situation</option>
                                    <option value="6">Go client</option>
                                    <option value="7">Paramétrage</option>
                                    <option value="8">Demande de lancement</option>
                                    <option value="9">Lancement</option>
                                    <option value="10">Optimisation</option>
                                    <option value="11">Envoi point</option>
                                    <option value="12">Bilan intermédiaire</option>
                                    <option value="13">Bilan final</option>
                                </select>
                            
                            </div>

                            
                         
                             
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>

    </div>

    <div class="test" style="display: none;">
        <p style="color: red;">test</p>
    </div>



      <div class="fox-popup-wrap" id="proj">
        
        <div class="fox-form-wrap">
            <div class="alert alert-success" role="alert">
               
              </div>
          <div>
              
            <div class="fox-close-btn">
                <div class="bar"></div>
                <div class="bar"></div>
              </div>
            <div class="col col-md-6">
              <div class="form-group">
                <div class="form-group">
                    <label for="tel">Date d'envoi</label>
                    <input class="form-control" id="date_env" name="date_env" type="date"/>
                  </div>
              </div>
            </div>
            <div class="col col-md-6">
              <div class="form-group">
                <label for="tel">Nombre de visuels</label>
                <input class="form-control" id="nb_vis" name="nb_vis" type="number"/>
              </div>
            </div>
            <div class="col col-md-6"></div>
            <div class="col col-md-6">
              <div class="form-group">
                <textarea name="comment" id="ann_projet" cols="80" rows="10" placeholder="Annotations"></textarea>
              </div>
            </div>
            <p class="btn_save" style="float: right;">Enregistrer</p>
        </div>

        </div>
      </div>
      









    <?php include "popup_diffus.php" ?>





      

    <?php include "pop_up_pro_visuel.php" ?>
 

    <?php include "popup_mod_visuel.php" ?>


    <?php include "popup_prop_setup.php" ?>
      

    <?php include "popup_mise_sit.php" ?>
   

    <?php include "popup_go_client.php" ?>
    <?php include "popup_parametrage.php" ?>
    <?php include "popup_optimisation.php" ?>
    <?php include "popup_envoie_point.php" ?>

    <?php include "popup_bilan_interm.php" ?>
    <?php include "popup_bilan_final.php" ?>
 
    <?php include "popup_modif_setup.php" ?>

    




    <!-- JS -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="vendor/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="vendor/jquery-steps/jquery.steps.min.js"></script>
    <script src="vendor/minimalist-picker/dobpicker.js"></script>
    <script src="vendor/nouislider/nouislider.min.js"></script>
    <script src="vendor/wnumb/wNumb.js"></script>
    <script src="js/main.js"></script>
</body>

</html>